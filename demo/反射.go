package main

import (
	"fmt"
	"reflect"
)

func setValue(obj interface{}, value interface{}) {
	v := reflect.ValueOf(obj)
	v2 := reflect.ValueOf(value)
	if v.Kind() != reflect.Ptr {
		panic("obj需为指针类型")
	}
	if v.Elem().Kind() != v2.Kind() {
		panic("指类型不统一")
	}
	switch v.Elem().Kind() {
	case reflect.String:
		v.Elem().SetString(v2.String())
	case reflect.Int:
		v.Elem().SetInt(v2.Int())
	}

}

func main() {

	name := "张三"
	age := 18
	setValue(&name, "李四")
	setValue(&age, 20)

	fmt.Println(name, age)
}
