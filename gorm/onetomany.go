package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"net/http"
)

var DB *gorm.DB
var mysqlLogger logger.Interface

type User struct {
	ID       uint      `json:"id" form:"id" gorm:"primarykey"`
	Name     string    `json:"name" form:"name"`
	Articles []Article `json:"articles" form:"articles"`
}
type Article struct {
	ID     uint   `json:"id" form:"id" gorm:"primarykey"`
	Title  string `json:"title" form:"title"`
	UserId uint   `json:"userId"`
	User   User   `json:"user"`
}

func init() {
	username := "root"
	password := "lc217917"
	host := "127.0.0.1"
	port := 3306
	dbName := "gorm"

	mysqlLogger = logger.Default.LogMode(logger.Info)

	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local", username, password, host, port, dbName)
	db, err := gorm.Open(mysql.New(mysql.Config{
		DSN:                       dsn,
		DefaultStringSize:         256,   // string 类型字段的默认长度
		DisableDatetimePrecision:  true,  // 禁用 datetime 精度，MySQL 5.6 之前的数据库不支持
		DontSupportRenameIndex:    true,  // 重命名索引时采用删除并新建的方式，MySQL 5.7 之前的数据库和 MariaDB 不支持重命名索引
		DontSupportRenameColumn:   true,  // 用 `change` 重命名列，MySQL 8 之前的数据库和 MariaDB 不支持重命名列
		SkipInitializeWithVersion: false, // 根据当前 MySQL 版本自动配置
	}), &gorm.Config{
		SkipDefaultTransaction: true, //跳过默认事务
		//Logger:                 mysqlLogger,
	})
	if err != nil {
		panic(err.Error())
	}
	DB = db.Session(&gorm.Session{
		Logger: mysqlLogger,
	})
	DB.AutoMigrate(&User{}, &Article{})
}

func main() {

	route := gin.Default()
	route.GET("/ua", func(context *gin.Context) {
		//创建用户，并且创建文章
		//var user = User{
		//	Name:     "小李",
		//	Articles: []Article{{Title: "go"}, {Title: "python"}},
		//}
		//DB.Create(&user)

		//创建文章，关联已有用户 创建文章js并关联到userid=1的用户
		//a := Article{Title: "js2"}
		//DB.Model(&User{ID: 1}).Association("Articles").Append(&a)

		//创建用户，关联已有文章 创建雅雅并把文章id等于2的关联到雅雅
		//u := User{Name: "雅雅"}
		//DB.Model(&Article{ID: 2}).Association("User").Append(&u)

		var user User
		DB.Preload("Articles").Take(&user, 2)
		context.JSON(http.StatusOK, user)
		//根据文章查用户
		//var a Article
		//DB.Preload("user").Find(&a)
		//context.JSON(http.StatusOK, a)
	})
	route.Run(":9000")

}
