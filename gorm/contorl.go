//package main
//
//import (
//	"fmt"
//	"github.com/gin-gonic/gin"
//	"gorm.io/driver/mysql"
//	"gorm.io/gorm"
//	"gorm.io/gorm/logger"
//	"net/http"
//	"time"
//)
//
//var DB *gorm.DB
//var mysqlLogger logger.Interface
//
//func init() {
//	username := "root"
//	password := "lc217917"
//	host := "127.0.0.1"
//	port := 3306
//	dbName := "gorm"
//
//	mysqlLogger = logger.Default.LogMode(logger.Info)
//
//	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local", username, password, host, port, dbName)
//	db, err := gorm.Open(mysql.New(mysql.Config{
//		DSN:                       dsn,
//		DefaultStringSize:         256,   // string 类型字段的默认长度
//		DisableDatetimePrecision:  true,  // 禁用 datetime 精度，MySQL 5.6 之前的数据库不支持
//		DontSupportRenameIndex:    true,  // 重命名索引时采用删除并新建的方式，MySQL 5.7 之前的数据库和 MariaDB 不支持重命名索引
//		DontSupportRenameColumn:   true,  // 用 `change` 重命名列，MySQL 8 之前的数据库和 MariaDB 不支持重命名列
//		SkipInitializeWithVersion: false, // 根据当前 MySQL 版本自动配置
//	}), &gorm.Config{
//		SkipDefaultTransaction: true, //跳过默认事务
//		//Logger:                 mysqlLogger,
//	})
//	if err != nil {
//		panic(err.Error())
//	}
//	DB = db.Session(&gorm.Session{
//		Logger: mysqlLogger,
//	})
//	DB.AutoMigrate(&User{})
//}
//func CreateUser(context *gin.Context) {
//
//	var user User
//	if err := context.ShouldBind(&user); err != nil {
//		context.JSON(http.StatusOK, gin.H{
//			"error": err.Error(),
//		})
//		return
//	}
//	err := DB.Create(&user).Error
//	if err != nil {
//		context.JSON(http.StatusOK, gin.H{
//			"msg": "创建失败",
//		})
//		return
//	}
//	context.JSON(http.StatusOK, user)
//}
//func CreateUsers(context *gin.Context) {
//	var users []User
//	if err := context.ShouldBind(&users); err != nil {
//		context.JSON(http.StatusOK, gin.H{
//			"error": err.Error(),
//		})
//		return
//	}
//	err := DB.Create(&users).Error
//	if err != nil {
//		context.JSON(http.StatusOK, gin.H{
//			"msg": "创建失败",
//		})
//		return
//	}
//	context.JSON(http.StatusOK, users)
//}
//
//type ApiUser struct {
//	ID   uint   `json:"id"form:"id"gorm:"primarykey"`
//	Name string `json:"name"form:"name"gorm:"unique,not null,size:15"`
//}
//
//func QueryUsers(context *gin.Context) {
//	var users []ApiUser
//	var query User
//	var err = context.ShouldBindQuery(&query)
//	if err != nil {
//		context.JSON(http.StatusOK, gin.H{
//			"msg": "查询失败",
//		})
//		return
//	}
//
//	er := DB.Model(&User{}).Where(query).Limit(10).Find(&users).Error
//	if er != nil {
//		context.JSON(http.StatusOK, gin.H{
//			"msg": "查询失败",
//		})
//		return
//	}
//	context.JSON(http.StatusOK, users)
//}
//
//func QueryUser(context *gin.Context) {
//	var id = context.Param("id")
//	var user User
//	err := DB.Take(&user, id).Error
//	if err != nil {
//		context.JSON(http.StatusOK, gin.H{"msg": "查询失败"})
//		return
//	}
//	context.JSON(http.StatusOK, user)
//}
//func UpdateUser(context *gin.Context) {
//	// 从 URL 参数中获取要更新的用户 ID
//	userID := context.Param("id")
//
//	// 查询要更新的用户
//	var user User
//	if err := DB.First(&user, userID).Error; err != nil {
//		context.JSON(http.StatusNotFound, gin.H{
//			"error": "该用户未查找到",
//		})
//		return
//	}
//
//	// 使用请求体中的数据更新用户字段
//	var updateUser User
//	if err := context.ShouldBindJSON(&updateUser); err != nil {
//		context.JSON(http.StatusBadRequest, gin.H{
//			"error": "Invalid data",
//		})
//		return
//	}
//
//	// 只更新传入的字段，保留其他字段的原始值
//	if err := DB.Model(&user).Updates(&updateUser).Error; err != nil {
//		context.JSON(http.StatusInternalServerError, gin.H{
//			"error": "Failed to update user",
//		})
//		return
//	}
//
//	context.JSON(http.StatusOK, user)
//}
//func DeleteUser(context *gin.Context) {
//	userIDStr := context.Param("id")
//	DB.Delete(&User{}, userIDStr)
//	context.JSON(http.StatusOK, gin.H{
//		"msg": "删除成功",
//	})
//}
//
//type User struct {
//	ID        uint           `json:"id"form:"id"gorm:"primarykey"`
//	CreatedAt time.Time      `json:"createdAt"form:"createdAt"`
//	UpdatedAt time.Time      `json:"updatedAt"form:"updatedAt"`
//	DeletedAt gorm.DeletedAt `json:"deletedAt"form:"deletedAt"gorm:"index"`
//	Name      string         `json:"name"form:"name"gorm:"unique,not null,size:15"`
//	Age       int            `json:"age"form:"age"gorm:"size:3"`
//	Gender    uint           `json:"gender"form:"gender"gorm:"size:3,default:1"`
//	Email     *string        `json:"email"form:"email"` //代表可以为空
//}
//
//func main() {
//
//	var router = gin.Default()
//	//新建用户
//	router.POST("user", CreateUser)
//	//批量创建用户
//	router.POST("users", CreateUsers)
//	//获取用户列表
//	router.GET("user", QueryUsers)
//	//根据id获取用户
//	router.GET("user/:id", QueryUser)
//	//根据id更新用户
//	router.PUT("user/:id", UpdateUser)
//	//根据id删除用户
//	router.DELETE("user/:id", DeleteUser)
//
//	router.Run(":9000")
//}
