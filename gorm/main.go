//package main
//
//import (
//	"fmt"
//	"github.com/gin-gonic/gin"
//	"gorm.io/driver/mysql"
//	"gorm.io/gorm"
//	"gorm.io/gorm/logger"
//	"net/http"
//	"time"
//)
//
//var DB *gorm.DB
//var mysqlLogger logger.Interface
//
//func init() {
//	username := "root"
//	password := "lc217917"
//	host := "127.0.0.1"
//	port := 3306
//	dbName := "gorm"
//
//	mysqlLogger = logger.Default.LogMode(logger.Info)
//
//	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local", username, password, host, port, dbName)
//	db, err := gorm.Open(mysql.New(mysql.Config{
//		DSN:                       dsn,
//		DefaultStringSize:         256,   // string 类型字段的默认长度
//		DisableDatetimePrecision:  true,  // 禁用 datetime 精度，MySQL 5.6 之前的数据库不支持
//		DontSupportRenameIndex:    true,  // 重命名索引时采用删除并新建的方式，MySQL 5.7 之前的数据库和 MariaDB 不支持重命名索引
//		DontSupportRenameColumn:   true,  // 用 `change` 重命名列，MySQL 8 之前的数据库和 MariaDB 不支持重命名列
//		SkipInitializeWithVersion: false, // 根据当前 MySQL 版本自动配置
//	}), &gorm.Config{
//		SkipDefaultTransaction: true, //跳过默认事务
//		//Logger:                 mysqlLogger,
//	})
//	if err != nil {
//		panic(err.Error())
//	}
//	DB = db.Session(&gorm.Session{
//		Logger: mysqlLogger,
//	})
//}
//
//// type 定义字段类型
//// size 定义字段大小
//// column 自定义列名
//// primaryKey 将列定义为主键
//// unique 将列定义为唯一键
//// default 定义列的默认值
//// not nul1 不可为空
//// embedded 嵌套字段
//// embeddedPrefix 嵌套字段前缀
//// comment 注释
//// 多个标签之前用 ； 连接
//type User struct {
//	ID        uint           `json:"id" form:"id" gorm:"primarykey"`
//	CreatedAt time.Time      `json:"createdAt" form:"createdAt"`
//	UpdatedAt time.Time      `json:"updatedAt" form:"updatedAt"`
//	DeletedAt gorm.DeletedAt `json:"deletedAt" form:"deletedAt" gorm:"index"`
//	Name      string         `json:"name" form:"name" gorm:"unique,not null,size:15"`
//	Age       int            `json:"age" form:"age" gorm:"size:3"`
//	Gender    uint           `json:"gender" form:"gender" gorm:"size:3,default:1"`
//	Email     *string        `json:"email" form:"email"` //代表可以为空
//}
//
//func createUser(context *gin.Context) {
//	user := User{}
//	if err := context.ShouldBind(&user); err != nil {
//		fmt.Println("error", err.Error())
//	}
//	err := DB.Create(&user).Error
//	if err != nil {
//		context.JSON(http.StatusOK, gin.H{"msg": "失败"})
//		return
//	}
//	context.JSON(http.StatusOK, user)
//}
//
//func createUsers(context *gin.Context) {
//	var studens []User
//	for i := 0; i < 10; i++ {
//		studens = append(studens, User{
//			Name: fmt.Sprintf("峰峰 %d", i),
//			Age:  21 + i,
//		})
//	}
//	err := DB.Create(&studens).Error
//	if err != nil {
//		context.JSON(http.StatusOK, gin.H{"msg": "失败"})
//		return
//	}
//	context.JSON(http.StatusOK, studens)
//}
//
//func getUsers(context *gin.Context) {
//	query := User{}
//	if err := context.BindQuery(&query); err != nil {
//		fmt.Println("error", err.Error())
//	}
//	fmt.Println("queryqueryquery", query.Name)
//	users := []User{}
//	result := DB.Where(&query).Find(&users)
//
//	context.JSON(http.StatusOK, gin.H{
//		"count": result.RowsAffected,
//		"data":  users,
//	})
//}
//func main() {
//	DB.AutoMigrate(&User{})
//	router := gin.Default()
//	router.POST("/user", createUser)
//	router.POST("/users", createUsers)
//
//	router.GET("/user", getUsers)
//	router.Run(":9000")
//}
