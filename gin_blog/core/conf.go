package core

import (
	"gin_blog/config"
	"gin_blog/global"
	"gopkg.in/yaml.v3"
	"io/ioutil"
)

func InitConf() {
	const ConfigFile = "settings.yaml"
	c := &config.Config{}
	yamlConf, err := ioutil.ReadFile(ConfigFile)
	if err != nil {
		panic(err.Error())
	}
	e := yaml.Unmarshal(yamlConf, c)
	if e != nil {
		panic(e.Error())
	}
	global.Config = *c
}
