package main

import (
	"fmt"
	"gin_blog/core"
	"gin_blog/global"
)

func init() {
	core.InitConf()
}

func main() {
	fmt.Println(global.Config)
}
