package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

func upload(context *gin.Context) {
	file, _ := context.FormFile("file")

	context.SaveUploadedFile(file, "./upload/"+time.Now().String()+file.Filename)
	context.JSON(http.StatusOK, gin.H{"msg": "上传成功"})
}
func uploads(context *gin.Context) {
	form, _ := context.MultipartForm()
	files := form.File["files"]
	for _, file := range files {
		context.SaveUploadedFile(file, "./upload/"+time.Now().String()+file.Filename)
	}
	context.JSON(http.StatusOK, gin.H{"msg": fmt.Sprintf("%d 个文件上传成功", len(files))})
}

func main() {
	router := gin.Default()

	router.POST("/upload", upload)
	router.POST("/uploads", uploads)

	router.Run(":8080")
}
