package main

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

func _query(context *gin.Context) {
	fmt.Println(context.Query("id"))
	fmt.Println(context.QueryArray("id"))
}

func _param(context *gin.Context) {
	fmt.Println(context.Param("id"))
	for _, param := range context.Params {
		fmt.Println(param.Key, param.Value)
	}
}

type Response[T any] struct {
	Code    int  `json:"code"`
	Success bool `json:"success"`
	Data    T    `json:"data"`
}

func bindJson[T any](context *gin.Context, obj T) (err error) {
	body, err := context.GetRawData()
	if err != nil {
		return err
	}
	json.Unmarshal(body, &obj)
	return
}

func _body(context *gin.Context) {
	type User struct {
		Id   string `json:"id"`
		Name string `json:"name"`
		Age  int    `json:"age"`
	}
	var user User
	bindJson(context, &user)
	res := Response[User]{
		Code:    http.StatusOK,
		Success: true,
		Data:    user,
	}
	context.JSON(http.StatusOK, res)
}

func main() {
	r := gin.Default()

	r.GET("/query", _query)
	r.GET("/param/:id/:bookid", _param)
	r.GET("/body", _body)

	r.Run(":8080")
}
