package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
	"net/http"
	"reflect"
)

type User struct {
	Name       string `json:"name" binding:"required,sign,min=2,max=5" msg:"名字校验失败"`
	Age        int    `json:"age" binding:"required,gte=18,lte=50" msg:"年龄校验失败"`
	Password   string `json:"password" binding:"required"  msg:"密码校验失败"`
	RePassword string `json:"rePassword" binding:"required,eqfield=Password" msg:"二次密码校验失败"`
}

func GetErrorMsg(err error, obj any) (msg string) {
	//断言为检验失败
	if errs, ok := err.(validator.ValidationErrors); ok {
		//遍历失败结果
		for _, fieldError := range errs {
			var t = reflect.TypeOf(obj)
			//fieldError.Field()拿到错误的key
			if f, exits := t.FieldByName(fieldError.Field()); exits {
				return f.Tag.Get("msg")
			}

		}
	}
	return

}
func b(context *gin.Context) {
	var user User
	var err = context.ShouldBindJSON(&user)
	if err != nil {
		var msg = GetErrorMsg(err, user)
		context.JSON(http.StatusOK, gin.H{"msg": msg, "error": err.Error()})
		return
	}
	context.JSON(http.StatusOK, user)
}
func main() {
	router := gin.Default()
	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {

		v.RegisterValidation("sign", func(fl validator.FieldLevel) (flag bool) {
			var namelist = []string{"lc", "111"}
			for _, s := range namelist {
				fmt.Println(s, fl.Field().String())
				if s == fl.Field().String() {
					return false
				}
			}
			return true
		})
	}
	router.GET("/", b)
	router.Run(":8080")
}
